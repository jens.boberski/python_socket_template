#!/bin/env python
'''
Server
'''
import logging
import argparse
import my_socket


def parse_arguments(args=None):

    parser = argparse.ArgumentParser(description='Starts a server')
    parser.add_argument('-p', '--port', help="port (default: 9051)", type=int, default=9051)
    parser.add_argument('--threads', help="threads used (default: 10)", type=int, default=10)
    parser.add_argument('--log', help="log file (default: stdout)", default=None)
    parser.add_argument('--debug', help="enable debuging logs", default=False, action='store_true')

    return parser.parse_args(args=args)




def test_process_input(input_dict):
    #    import time
    #time.sleep(1)
    return {"server says: " : input_dict['file']}




def start_server(port, threads):
    my_socket.server(port, test_process_input, threads = threads)



if __name__ == '__main__':
    ARGS = parse_arguments()
    FORMAT = "[%(levelname)s] %(asctime)-15s - %(name)s: %(message)s"

    if ARGS.debug:
        LEVEL = logging.DEBUG
    else:
        LEVEL = logging.INFO

    logging.basicConfig(level=LEVEL, format=FORMAT, filename=ARGS.log)

    start_server(ARGS.port, ARGS.threads)
