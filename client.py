#!/bin/env python
# client.py
import argparse
import sys
import my_socket
import json


##########################################################################################


def parse_arguments(args=None):
    parser = argparse.ArgumentParser(description='Starts a server')
    parser.add_argument('-p', '--port', help="port (default: 9051)", type=int, default=9051)
    parser.add_argument('--host', help="host (default: localhost)", default='localhost')
    parser.add_argument('--file')

    return parser.parse_args(args=args)



def make_request():
    args = parse_arguments()

    send_dict = {'file': my_socket.file_to_string(args.file)}
    result = my_socket.client_request(args.host, args.port, send_dict)

    print(result)




if __name__ == '__main__':
    make_request()
