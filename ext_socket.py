import binascii

PREFIX_BLOCK = "--BEGINBLOCK--".encode('utf-8')
SUFFIX_BLOCK = "--ENDBLOCK--".encode('utf-8')
LABEL_CONTENT_DIVIDER = "--LABEL-CONTENT--".encode('utf-8')



def recvall_dict(sock, buffer_size=4096):
    return blocks_to_dict(recvall(sock, buffer_size))


def sendall_dict(sock, dictionary):
    blocks = dict_to_blocks(dictionary)
    sock.sendall(blocks)



def get_file_content(filename):
    with open (filename, "rb") as filestream:
        filecontent = filestream.read()

    return filecontent.decode('utf-8')



def recvall_collector(sock, buffer_size):
    buf = sock.recv(buffer_size)
    while buf:
        yield buf
        if len(buf) < buffer_size:
            break
        buf = sock.recv(buffer_size)



def recvall(sock, buffer_size=4096):
    response = b''.join(recvall_collector(sock, buffer_size))
    return response



def remove_suffix(input_string, suffix):
    thelen = len(suffix)
    if input_string[-thelen:] == suffix:
        return input_string[:-thelen]
    return input_string



def string_to_block(input_string, label):

    return PREFIX_BLOCK + label.encode('utf-8') + LABEL_CONTENT_DIVIDER + binascii.b2a_base64(input_string.encode('utf-8')) + SUFFIX_BLOCK



def block_to_key_value(block):
    bytes_label, bincontent_suf = block.split(LABEL_CONTENT_DIVIDER)

    label = bytes_label.decode('utf-8')

    bincontent = remove_suffix(bincontent_suf, suffix=SUFFIX_BLOCK)
    content = binascii.a2b_base64(bincontent).decode('utf-8')

    return label, content



def dict_to_blocks(input_dict):
    ret_val = "".encode('utf-8')

    for key, value in input_dict.items():
        ret_val += string_to_block(str(value), key)

    return ret_val



def blocks_to_dict(input_string):
    rv = {}

    blocks = input_string.split(PREFIX_BLOCK)[1:]

    for block in blocks:
        label, content = block_to_key_value(block)
        rv[label] = content


    return rv
