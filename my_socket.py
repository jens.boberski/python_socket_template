import hashlib
import time
import logging
import socket
import json
import threading


MAX_TRIES = 10
PACKET_SIZE = 4096


MSG_BEGIN = hashlib.sha256('STARTOFMESSAGE'.encode('utf-8')).hexdigest().encode('utf-8')
MSG_END = hashlib.sha256('ENDOFMESSAGE'.encode('utf-8')).hexdigest().encode('utf-8')
MSG_GOOD = hashlib.sha256('GOODMESSAGE'.encode('utf-8')).hexdigest().encode('utf-8')
MSG_BAD = hashlib.sha256('BADMESSAGE'.encode('utf-8')).hexdigest().encode('utf-8')
SESSION_FAILED = hashlib.sha256('BADSESSION'.encode('utf-8')).hexdigest().encode('utf-8')



def calc_hash(s):
    return hashlib.sha256(s).hexdigest().encode('utf-8')



def split_string(s, w):
    return [s[i:i + w] for i in range(0, len(s), w)]



def file_to_string(filename):
    with open (filename, "rb") as filestream:
        filecontent = filestream.read()

    return filecontent.decode('utf-8')



def recv_packet(sock):
    while True:
        raw_msg = sock.recv(PACKET_SIZE)

        expexted_hash_front = raw_msg[:64]
        expexted_hash_back = raw_msg[-64:]

        if expexted_hash_front != expexted_hash_back:
            #print("Non matching hashes (front/back)")
            #print(expexted_hash_front)
            #print(expexted_hash_back)
            sock.send(MSG_BAD)
            continue

        msg = raw_msg[64:-64]
        found_hash = calc_hash(msg)

        if expexted_hash_front != found_hash:
            #print("Non matching hashes (front/content)")
            sock.send(MSG_BAD)
            continue

        if expexted_hash_front == found_hash and expexted_hash_front == expexted_hash_back:
            #print("Good packet received")
            sock.send(MSG_GOOD)
            return msg



def recv_long_string(sock):
    active_transmition = False
    while True:
        packet = recv_packet(sock)

        if active_transmition and packet == MSG_END:
            break


        if packet.startswith(MSG_BEGIN):
            expected_packets = packet[len(MSG_BEGIN):]
            #print("expecting {}".format(expected_packets))
            data_packets = []#[None] * expected_packets
            active_transmition = True
            continue

        if active_transmition:
            data_packets.append(packet.decode('utf-8'))

    return "".join(data_packets)



def send_packet(sock, packet, max_tries):
    packet_hash = calc_hash(packet)

    tries = 1
    while True:
        if tries > max_tries:
            print('max tries reached')
            return False
        #print('sending {}... (try: {})'.format(packet, tries))
        try:
            sock.sendall(packet_hash+packet+packet_hash)
        except Exception as e:
            time.sleep(0.1)
            print(e)
        reply = sock.recv(64)
        #print("Server reply after message send: {}".format(reply))
        if reply == MSG_GOOD:
            #print('success')
            break
        else:
            pass
            #print(reply)
        tries += 1

    return True



def send_long_string(sock, data_string):
    packets = split_string(data_string, PACKET_SIZE - 200)
    prolog = MSG_BEGIN + str(len(packets)).encode('utf-8')

    epilog = MSG_END

    send_packet(sock, prolog, 10)

    for idx,packet in enumerate(packets):
        send_packet(sock, packet, 10)

    send_packet(sock, epilog, 10)



##server
def server(port, answer_function, threads = 5):
    '''answer_function takes a dict as argument and returns a dict again'''''
    log = logging.getLogger("server")
    hostname = socket.gethostname()
    log.info("Starting server at {}:{}".format(hostname, port))

    soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # this is for easy starting/killing the app
    soc.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    soc.bind(('', port))

    soc.listen(1)

    pool = threading.Semaphore(threads)

    while True:
        conn, addr = soc.accept()
        try:
            threading.Thread(target = listen_to_client,args = (pool, conn, addr, answer_function, log)).start()
            conn.settimeout(60)

        except Exception as exception:
            log.error("Error while starting processing thread from {}: {}".format(addr, exception))
            #raise

    soc.close()



def listen_to_client(pool, conn, addr, answer_function, log):
    pool.acquire()
    input_string_from_client = recv_long_string(conn)
    log.info("Request from{}".format(addr))
    log.debug("Received input from {}: {}".format(addr, input_string_from_client))
    input_dict = json.loads(input_string_from_client)
    result_dict = answer_function(input_dict)
    result_string = json.dumps(result_dict).encode('utf-8')
    log.debug("Created reply to {}: {}".format(addr, result_string))
    send_long_string(conn, result_string)
    conn.close()
    pool.release()





def client_request(host, port, input_dict):

    message = json.dumps(input_dict)

    soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    soc.connect((host, port))

    send_long_string(soc, message.encode('utf-8'))
    answer_string = recv_long_string(soc)
    return json.loads(answer_string)



##USAGE

#server
#======
#import my_socket
#
#def test(input_dict):
#    return {....}
#
#my_socket.server(8914, test)


#client
#======
#import my_socket
#
#my_socket.send_request('localhost', 8914, my_dict)
